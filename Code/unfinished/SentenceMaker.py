#! /bin/env/ python3
# Aalok S.

############################################################
###	This program may be used to create language models	####
###	off of an input text file or standard input,		####
###	and subsequently generate random text using the		####
###	said language model.								####
###	We make use of KenLM bundled with Moses MT system	####
###	to create language models for us, and then do all	####
###	else locally.										####
############################################################

###	For a command-loop design
from cmd import Cmd
###	For statistics and pseudo-random generation
import math
import numpy
###	To call bash commands from within
import subprocess

class TextMaker(Cmd):

	model_exists = False
	
	def __init__(self, *args):
		super(TextMaker, self).__init__()
	
	def do_generate(self, arg):
		"""Create a sentence based on provided input file"""
		try:
			arg = arg.split(',')
			
			print(arg)
			#raise SystemExit
			
			if len(arg) != 2:
				raise SyntaxError
				
			test_file = open(arg[0], 'r')
			test_file.close()
			
			if not self.model_exists:
				self.model_exists = True
				arg += [str(arg[0] + ".lm%d.arpa"%int(arg[1]))]
				print(arg)
				subprocess.call(["./mosesMT/bin/lmplz", "-o", "%d"%int(arg[1])], input=open(arg[0], 'r'), stdout=open(arg[2], 'r'))
			
			print("after subprocess")
			
			model = open(arg[2], 'r')
			
			model_dict = {}
			dummy = model.readline()
			
			for i in range(1,arg[1]+1):
				model_dict[i] = {}
				model_dict[i]['~'] = model.readline().split()[1].split('=')[1]
			
			dummy = model.readline()
			
			for i in range(1, arg[1]+1):
				dummy = model.readline()
				for j in range(model_dict[i]['~']):
					line_parts = model.readline().split()
					if i == 1:
						model_dict[i][None] = model_dict[i].get(None, {})
						model_dict[i][None][line_parts[1]] = line_parts[0]
					else:
						k_gram = ''
						for k in range(1, i):
							k_gram += line_parts[k]
						model_dict[i][k_gram] = model_dict[i].get(k_gram, {})
						model_dict[i][k_gram][line_parts[i]] = line_parts[0]
						
				dummy = model.readline()
			
			sentence = ''
			next_token = ''
			while(next_token != '</s>'):
				next_token = self.get_next_token(model_dict, sentence, arg[1])
				sentence += next_token
				print(sentence,'\n')
				
		except Exception:
			print("Error: unable to read file: %s. Please make sure to provide correct path."%arg[0])
			
	
	def do_help(self, arg):
		"""Shows a helpful usage message"""
		print("This program may be used to generate text based off of an input file.")
		print("To use, type `generate </path/to/input/file.txt>,N' where N>0 is the length of an N-gram")
		print("Please be sure to include the comma and leave no whitespace.")
		print("To exit, type exit and hit return.")
		
	def do_exit(self, arg):
		"""Exits the program."""
		print ("Quitting.")
		raise SystemExit
		
	def get_next_token(self, model_dict, sentence, max_n):
		possibilities = None
		weights = None
		key = None
		if len(sentence.split()):
			parts = sentence.split()[max(len(sentence.split())-max_n,0):]
			if len(parts):
				key = ''
				for part in parts:
					key += part
		
		possibilities = list(model_dict[1][None].keys())
		weights = list(model_dict[1][None].values())
		weights = [x/sum(weights) for x in weights]
		
		return numpy.random.choice(possibilities, 1, p=weights)
		
		
if __name__ == '__main__':
    prompt = TextMaker("")
    prompt.prompt = ':: '
    try:
    	prompt.cmdloop('''
    Coded by Aalok S. on 2018-01-25.
    This program comes with ABSOLUTELY NO WARRANTY.
    This is free software, and you are welcome to redistribute it
    under certain conditions. See the GNU GPL v3.
    Please type ``help" for usage directions.''')
    except KeyboardInterrupt:
    	print("\nExiting due to KeyboardInterrupt")
    	raise SystemExit
