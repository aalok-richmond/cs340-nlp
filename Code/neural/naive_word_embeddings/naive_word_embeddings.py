#! /bin/env/ python3

########################################################
####    Preamble

import os

import pickle

import keras
from keras.models import Sequential
from keras.layers import Dense, Activation

import itertools
import numpy
import random
numpy.random.seed(7)

from progress_printer import Progress_printer


########################################################
####    Parameters

N = ngram_size = 3 # skip-gram size = N-1
D = num_dimensions = 30
V = vocab_size = None
one_hots = None
num_epochs = 2**7

in_directory = "./../../../Data/Gutenberg/txt/"

########################################################
####    Read input file(s) and tokenize
print("\nRead input file(s) and tokenize")

all_tokens_set = set()
all_tokens = list()

pp = Progress_printer()
file_list = os.listdir(in_directory)
random.shuffle(file_list)

iteration = 0
for text_file_name in file_list:
    try:
        pp.print_progress(iteration, len(file_list)*0+100)
        iteration += 1
        
        text_file = open(in_directory + text_file_name, 'r',  encoding='latin-1')
        new_tokens = text_file.read().lower().split()
        
        all_tokens += new_tokens
        #all_tokens_set.update(new_tokens)
        
        if iteration > 100:# or len(all_tokens) >= 10000:
            break
    except IOError:
        print(IOError, "while opening input file: %s" % (in_path))
        raise SystemExit

all_tokens = all_tokens[:10000]
all_tokens_set = set(all_tokens)

token_to_index = dict()
index_to_token = dict()

iteration_index = 0
for token in all_tokens_set:
    token_to_index[token] = iteration_index
    index_to_token[iteration_index] = token
    iteration_index += 1

V = len(set(all_tokens)) # updating vocab size
ngrams_list = list(zip(*[all_tokens[i:] for i in range(N)])) # list unpacking operator

#print(index_to_token.keys()) #DEBUG

########################################################
####    Create one-hot encodings and training dataset
print("\nCreate one-hot encodings and training dataset")

one_hots = keras.utils.to_categorical(list(index_to_token.keys()), num_classes=V)

#print(one_hots) #DEBUG

#print(*[ (ngram, list(ngram)[:N//2]+list(ngram)[N//2+1:]+[list(ngram)[N//2],] ) for ngram in ngrams_list]) #DEBUG

skip_gram = lambda ngram, N: list(itertools.chain.from_iterable([one_hots[token_to_index.get(token)] for token in list(ngram)[:N//2]+list(ngram)[N//2+1:]+[list(ngram)[N//2],] ]) )


#print(skip_gram(("a", "of", "the", "if", "or"), 5)) #DEBUG

#print([ skip_gram(ngram, N) for ngram in ngrams_list]) #DEBUG
#print([ (ngram, list(ngram[:N//2]) + list(ngram[N//2+1:]) + [ngram[N//2]]) for ngram in ngrams_list]) #DEBUG

dataset = numpy.array([skip_gram(ngram, N) for ngram in ngrams_list[:-25] ])

print(dataset, "dataset") #DEBUG

X = dataset[:,0      :V*(N-1)] # skipgram context(s)
Y = dataset[:,V*(N-1):       ] # skipgram skipped word(s)

print ("x", X, "\ny", Y) #DEBUG

########################################################
####    Stack model, layer-by-layer
print("\nStack model, layer-by-layer")

try:
    model = keras.models.load_model("!skipgram_model_%d" % num_epochs)
except:
    model = Sequential()

    model.add(Dense(D, init='uniform', input_dim = (N-1)*V, activation='relu'))
    model.add(Dense(V, init='uniform', activation='softmax'))

    model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])

    ########################################################
    ####    Train model
    print("\nTrain model")

    model.fit(X, Y, epochs=num_epochs, batch_size=16,)

    model.save("skipgram_model_%d" % num_epochs)

########################################################
####    See output for arbitrary words
print("\nSee output for arbitrary words")

def vector_sq_distance(v1, v2):
    dist = 0.0
    for dim in range(len(v1)):
        dist += (v1[dim] - v2[dim]) ** 2
    return dist

testdataset = numpy.array([skip_gram(ngram, N) for ngram in ngrams_list[-25:]])

testX = testdataset[:,0      :V*(N-1)] # skipgram context(s)
testY = testdataset[:,V*(N-1):       ] # skipgram skipped word(s)

test_gram_list = []
for vec in testX:
    testgrams = []
    for token in range(N-1):
        testgrams.append(index_to_token.get(numpy.argmax(vec[token*V:(token+1)*V])) )
    test_gram_list.append(testgrams)
test_gram_list = numpy.array(test_gram_list)

predictions = numpy.array(model.predict(testX))
rounded = [index_to_token.get(index) for index in list(itertools.chain.from_iterable([p.argsort()[-5:][::-1] for p in predictions]))]

results = list(zip(test_gram_list[:,:(N-1)//2], [index_to_token.get(numpy.argmax(vector)) for vector in testY], test_gram_list[:,(N-1)//2:], [rounded[i:i+5] for i in range(0,len(rounded), 5)]) )

for result in results:
    print (result)

# evaluate the model
scores = model.evaluate(testX, testY)
print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

