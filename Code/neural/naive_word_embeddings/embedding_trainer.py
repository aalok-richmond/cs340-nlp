#! /bin/env/ python3

########################################################
####    Preamble

import os

import pickle
import collections

import keras
from keras.models import Sequential, Model
from keras.layers import Dense, Activation, Embedding, Input, Reshape

import itertools
import numpy
import random
numpy.random.seed(7)

from progress_printer import Progress_printer


##  This class may be used to train word embeddings using skip-grams
class EmbeddingTrainer:

    ########################################################
    ####    Parameters

    ##  Defaults.
    N = ngram_size = 3 # skip-gram size = N-1
    D = num_dimensions = 30
    V = vocab_size = None
    #one_hots = None
    num_epochs = 2**8
    in_directory = "./../../../Data/Gutenberg/txt/"
    pp = Progress_printer()

    ##  Class constructor
    def __init__(self, **kwargs):
    
        self.ngram_size = kwargs.get("ngram_size", self.ngram_size)
        self.num_dimensions = kwargs.get("num_dimensions", self.num_dimensions)
        self.num_dimensions = kwargs.get("num_dimensions", self.num_dimensions)
        self.num_epochs = kwargs.get("num_epochs", self.num_epochs)
    
        self.tokens = list()
        
        self.file_list = os.listdir(kwargs.get("in_directory", self.in_directory))
        random.shuffle(self.file_list)
    
        iteration = 0
        for text_file_name in self.file_list:
            try:
                self.pp.print_progress(iteration, len(self.file_list)*0+100)
                iteration += 1
                
                text_file = open(self.in_directory + text_file_name, 'r',  encoding='latin-1')
                new_tokens = text_file.read().lower().split()
                self.tokens += new_tokens
                
                if iteration > 100:
                    break
            except IOError:
                print(IOError, " while opening input file: %s" % (in_path))
                raise SystemExit
    
        self.token_indices, self.vocab, self.token_to_index, self.index_to_token = self.build_dataset(self.tokens, 1000)
        

    ########################################################
    ####    Read input file(s) and tokenize
    
    def build_dataset(self, words, threshold_rank):
        """Process raw inputs into a dataset."""
    
        print("\nTokenize and build dataset")

        count = [['UNK', 0]]
        count.extend(collections.Counter(words).most_common(threshold_rank - 1))
        self.V = self.vocab_size = len(count) # updating vocab size
        
        one_hot_index = dict()
        for word, _ in count:
            one_hot_index[word] = len(one_hot_index)
            
        data = list()
        for word in words:
            if word in one_hot_index:
                index = one_hot_index[word]
            else:
                index = 0  # by convention, dictionary['UNK']
                count[0][1] += 1
            data.append(index)
            
        reversed_dictionary = dict(zip(one_hot_index.values(), one_hot_index.keys()))
        
        return data, count, one_hot_index, reversed_dictionary

        
    def construct_model(self):
        ########################################################
        ####    Create layers and model
        

        self.valid_size = 16     # Random set of words to evaluate similarity on.
        self.valid_window = 100  # Only pick dev samples in the head of the distribution.
        self.valid_examples = numpy.random.choice(self.valid_window, self.valid_size, replace=False)
        
        print("\nCreate one-hot encodings and training dataset")

        self.sampling_table = keras.preprocessing.sequence.make_sampling_table(self.vocab_size)
        couples, labels = keras.preprocessing.sequence.skipgrams(self.token_indices, self.vocab_size, window_size=self.ngram_size, sampling_table=self.sampling_table)
        self.couples, self.labels = couples, labels

        target_words, context_words = zip(*couples)
        target_words = numpy.array(target_words, dtype="int32")
        context_words = numpy.array(context_words, dtype="int32")
    
        self.target_words, self.context_words = target_words, context_words
    
        embedding = Embedding(self.vocab_size, self.num_dimensions, input_length=1, name='embedding')

        input_target = Input((1,))
        input_context = Input((1,))

        target = embedding(input_target)
        target = Reshape((self.num_dimensions, 1))(target)
        context = embedding(input_context)
        context = Reshape((self.num_dimensions, 1))(context)

        similarity = keras.layers.merge([target, context], mode='cos', dot_axes=0)
        dot_product = keras.layers.merge([target, context], mode='dot', dot_axes=1)
        dot_product = Reshape((1,))(dot_product)
        
        output = Dense(1, activation='sigmoid')(dot_product)

        # create the primary training model
        self.model = Model(input=[input_target, input_context], output=output)
        self.model.compile(loss='binary_crossentropy', optimizer='rmsprop')
    
        self.validation_model = Model(input=[input_target, input_context], output=similarity)
        
        
    ########################################################
    ####    Training
        
    def train(self):
        arr_1 = numpy.zeros((1,))
        arr_2 = numpy.zeros((1,))
        arr_3 = numpy.zeros((1,))
        for cnt in range(self.num_epochs):
            idx = numpy.random.randint(0, len(self.labels)-1)
            arr_1[0,] = self.target_words[idx]
            arr_2[0,] = self.context_words[idx]
            arr_3[0,] = self.labels[idx]
            loss = self.model.train_on_batch([arr_1, arr_2], arr_3)
            #if i % 100 == 0:
            #    print("Iteration {}, loss={}".format(cnt, loss))
            if cnt % 10000 == 0:
                self.run_sim()
        
        for i in range(90,100):
            print (i, self.index_to_token[i])
    
        pickle.dump(index_to_token, open("word2vec_vocab:%d_epochs:%d.dict" % (self.vocab_size, self.num_epochs), "wb"))
        self.model.save("word2vec_vocab:%d_epochs:%d.hdf" % (self.vocab_size, self.num_epochs))
      
    def run_sim(self):
        for i in range(self.valid_size):
            valid_word = self.index_to_token[self.valid_examples[i]]
            top_k = 8  # number of nearest neighbors
            sim = self._get_sim(self.valid_examples[i])
            nearest = (-sim).argsort()[1:top_k + 1]
            log_str = 'Nearest to %s:' % valid_word
            for k in range(top_k):
                close_word = self.index_to_token[nearest[k]]
                log_str = '%s %s,' % (log_str, close_word)
            print(log_str)

    def _get_sim(self, valid_word_idx):
        sim = numpy.zeros((self.vocab_size,))
        in_arr1 = numpy.zeros((1,))
        in_arr2 = numpy.zeros((1,))
        for i in range(self.vocab_size):
            in_arr1[0,] = valid_word_idx
            in_arr2[0,] = i
            out = self.validation_model.predict_on_batch([in_arr1, in_arr2])
            sim[i] = out
        return sim
        
                
        
t = trainerInstance = EmbeddingTrainer()
t.construct_model()
t.train()
           

           
