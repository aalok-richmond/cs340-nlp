#! /bin/env/ python3

import keras
from keras.models import Sequential
from keras.layers import Dense, Activation

import numpy
import random
numpy.random.seed(7)


#dataset = numpy.loadtxt("pima-diabetes.data.csv", delimiter=",")

R = 10
N = 2
f = lambda x: x % 2 #int(x**.5)**2

r = list(range(0,R))
random.shuffle(r)

dataset = numpy.array([(numpy.array(i), f(i)) for i in r*5 ] )
X = dataset[:,0]
Y = dataset[:,1]
y = keras.utils.to_categorical(Y, num_classes=N)

#print (list(zip(dataset[:,0:8], dataset[:,8]))[:3])
print (X,Y,y)

#raise SystemExit

# create model

model = Sequential()
model.add(Dense(1+64+1024, init='normal', input_dim=1, activation='tanh'))
model.add(Dense(1+64, init='normal', activation='relu'))
model.add(Dense(1+64, init='normal', activation='relu'))
model.add(Dense(1+64, init='normal', activation='relu'))
model.add(Dense(1+64, init='normal', activation='relu'))
model.add(Dense(N, init='uniform', activation='softmax'))


#model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])

model.fit(X, y, epochs=1024*3, batch_size=1,)

dataset = numpy.array([(numpy.array(i), f(i)) for i in range(0,R)])
X = dataset[:,0]
Y = dataset[:,1]
y = keras.utils.to_categorical(Y, num_classes = len(y[0]))

predictions = model.predict(X)
rounded = [numpy.argmax(arr) for arr in predictions]

print(list(zip(X, rounded)))

# evaluate the model
scores = model.evaluate(X, y)
print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

