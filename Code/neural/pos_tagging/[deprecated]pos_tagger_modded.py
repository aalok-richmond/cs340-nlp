#!/usr/bin/env python3
################################################################################
####    Preamble    ############################################################
print("Section: preamble")

import keras as K
from keras import layers as kl
import numpy

import os
import io
#import progressbar

import time
import datetime


################################################################################
####    Define and prepare input    ############################################
print("Section: define and prepare input")

max_docs = 50
base_path = "../spacy/tagged"


vocab = set()
token_to_index = dict()
index_to_token = dict()

pos_tags = set()
tag_to_index = dict()
index_to_tag = dict()

for fname in os.listdir(base_path)[:max_docs]:#progressbar.progressbar(os.listdir(base_path)[:max_docs]):
    for line in io.open(os.path.join(base_path, fname), "r", errors = 'ignore'):
        splitline = line.lower().split()
        if len(line) <= 1 or len(splitline) <= 2:
            continue
        vocab.update(splitline[0:1])
        pos_tags.update(splitline[1:2])


print() # Newline

index = 1
for token in vocab:
    token_to_index[token] = index
    index_to_token[index] = token
    index += 1

index = 0
for tag in pos_tags:
    tag_to_index[tag] = index
    index_to_tag[index] = tag
    index += 1


################################################################################
####    Define and compile model    ############################################
print("Section: define and compile model")

model = K.models.Sequential()

#input_layer = kl.Input(shape=(1,))
#model.add(input_layer)

model.add(kl.Embedding(len(vocab), 256, input_length=3))
model.add(kl.Flatten())
model.add(kl.Dense(len(vocab), activation="relu"))
#model.add(kl.Dropout(.3, noise_shape=None, seed=None))
model.add(kl.Dense(len(pos_tags), activation="softmax"))

model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["categorical_accuracy"])

print(model.summary())
################################################################################
####    Train model by iteratively reading tokens from input
print("Section: train model by iteratively reading tokens from input")

def data_generator(start=0,end=max_docs):

    fist_token = 0
    second_token = 0; second_tag = 0
    third_token = 0; third_tag = 0

    for fname in os.listdir(base_path)[start:end]:#progressbar.progressbar(os.listdir(base_path)[start:end]):

        for line in io.open(os.path.join(base_path, fname), "r", errors = 'ignore'):

            splitline = line.lower().split()
            if len(line) <= 1 or len(splitline) <= 2:
                continue

            first_token = second_token
            second_token = third_token
            third_token = token_to_index.get(splitline[0],0)

            second_tag = third_tag
            third_tag = tag_to_index.get(splitline[1],0)

            x_batch = numpy.zeros((1,3))
            x_batch[0] = numpy.array((first_token, second_token, third_token))

            y_batch = numpy.zeros((1,len(pos_tags)))
            y_batch[0,second_tag] = 1

            #print((x_batch, y_batch), (x_batch.shape, y_batch.shape))

            #yield (numpy.zeros((47,12)), numpy.zeros((12,1)))
            yield (x_batch, y_batch)

            #model.fit(x=x_batch, y=y_batch, batch_size=1, epochs=3, verbose=1)

# Train model using generator syntax
model.fit_generator(data_generator(), steps_per_epoch=100, epochs=10)

x_batch = next(data_generator())[0]
print(model.predict(x_batch))

# Save model after done training

timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M')
model.save("model_%s.kerasmodel" %(timestamp))

################################################################################
####    Evaluate model against remaining data
print("Section: evaluate model against remaining data")

model.evaluate_generator(data_generator(max_docs, max_docs+10), steps=100)


test = ["I", "like", "FOSS"]

x_batch = numpy.zeros((1,3))
x_batch[0] = numpy.array((token_to_index.get(test[0],0), token_to_index.get(test[1],0), token_to_index.get(test[2],0)))
predictions = model.predict(x_batch)[0]
print(predictions, "\n", test, [index_to_tag.get(x,0) for x in numpy.argsort(-predictions)[:3]])

#model.predict_generator(data_generator(max_docs, len(os.listdir(base_path))), steps=100)
