#! /bin/env/ python3

import os
import pickle

base_path = "../spacy/tagged"
pickle.dump(os.listdir(base_path), open('files_list.pickle', 'wb'))
