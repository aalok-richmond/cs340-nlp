#! /bin/env/ python3

import io
import keras
from keras import layers as kl
import numpy
import os
import pickle
os.chdir(os.path.dirname(os.path.abspath('__file__')))

# import os
# import io
# #import progressbar
#
import time
import datetime


print("Section: define and prepare input")

max_docs = 10
base_path = "../spacy/tagged"
static_files_list = pickle.load(open('files_list.pickle', 'rb'))

vocab = set()
token_to_index = dict()
index_to_token = dict()

pos_tags = set()
tag_to_index = dict()
index_to_tag = dict()

for fname in static_files_list[:max_docs]:
    for line in io.open(os.path.join(base_path, fname), "r"):
        splitline = line.lower().split()
        if len(line) <= 1 or len(splitline) <= 2:
            continue
        vocab.update(splitline[0:1])
        pos_tags.update(splitline[1:2])


print() # Newline

index = 1
for token in vocab:
    token_to_index[token] = index
    index_to_token[index] = token
    index += 1

index = 0
for tag in pos_tags:
    tag_to_index[tag] = index
    index_to_tag[index] = tag
    index += 1


timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M')
for name, object in [
        ('pos_tags', pos_tags),
        ('vocab', vocab),
        ('token_to_index', token_to_index),
        ('index_to_token', index_to_token),
        ('tag_to_index', tag_to_index),
        ('index_to_tag', index_to_tag),
    ]:
    # print(name, object)
    # with open("dictionaries/" + name + "_" + timestamp, 'wb') as fileob:
    #     pickle.dump(object, fileob)

###############################################################################

# Load saved model
model = keras.models.load_model("model_2018-06-09_12-09.kerasmodel")

def index(tokens):
    return [token_to_index.get(token, 0) for token in tokens]

sentences = [
    'I eat apples',
    'Who are you?',
    'it was time',
    'three plus four',
    'where art thou',
    'he will go',
    'you are dead',

    'the candle looked',
    'than the darkness'
]

for sentence in sentences:
    x_batch = numpy.zeros((1,3))
    x_batch[0] = numpy.array(( index(sentence.split()) ))
    print (x_batch)

    # y_batch = numpy.zeros((1,len(pos_tags)))
    # y_batch[0,second_tag] = 1
    prediction = model.predict(x_batch)
    print(sentence, "\n", index_to_tag[numpy.argmax(prediction)])

big_sentences = [
    "the gloomy hall with the shadows thrown by the candles looked hardly more inviting",
]

for sentence in big_sentences:
    tags = [[],[]]
    for word_index in range(len(sentence.split())):
        window = index(sentence.lower().split()[max(word_index-1,0):word_index+1+1])
        if word_index == 0:
            window = [0] + window
        if word_index+1 == len(sentence.split()):
            window.append(0)
        print(window)
        x_batch = numpy.zeros((1,3))
        x_batch[0] = numpy.array(( window ))
        print(x_batch)

        # y_batch = numpy.zeros((1,len(pos_tags)))
        # y_batch[0,second_tag] = 1
        prediction = model.predict(x_batch)
        # print(sentence, "\n", index_to_tag[numpy.argmax(prediction)])
        tags[0].append(index_to_tag[numpy.argmax(prediction)])
        tags[1].append(index_to_tag[numpy.argmin(prediction)])
    print(sentence.split(), "\n",index(sentence.split()), "\n", tags)

keras.backend.clear_session()
