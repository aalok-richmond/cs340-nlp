#! /bin/env/ python3

import pickle
import pandas as pd
import numpy as np
import hypertools as hyp

import os
os.chdir(os.path.dirname(os.path.abspath('__file__')))
os.getcwd()


# char_embeddings = np.array((128,64))
# with open('ascii_embeddings_64.pickle', 'rb') as file:
#     char_embeddings = np.array(pickle.load(file)[1:129])

data = pd.read_csv('99999word-embedding.csv')
# print(data[0])
data.head()

# for idx,x in enumerate(char_embeddings):
#     print (idx,x)#[chr(x) for x in range(128)][idx])
#     break
#
# labels = [chr(x) for x in range(128+1)] + [None for x in range(128)]

hyp.plot(data, '.')#, legend=[chr(x) for x in range(128)], reduce='PCA')
