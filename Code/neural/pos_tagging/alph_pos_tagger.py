###############################################################################
####	Pramble    ############################################################
print("Section: preamble")

import keras as K
from keras import layers as kl
import numpy

import os
import io
#import progressbar

import time
import datetime

import pickle


################################################################################
####    Define and prepare input    ############################################
print("Section: define and prepare input")

max_docs = 10
base_path = "../spacy/tagged"
static_files_list = pickle.load(open('files_list.pickle', 'rb'))

# vocab = set()
# token_to_index = dict()
# index_to_token = dict()

# pos_tags = set()
pos_tags = pickle.load(open('dictionaries/pos_tags_2018-06-07_19-54', 'rb'))
# tag_to_index = dict()
tag_to_index = pickle.load(open('dictionaries/tag_to_index_2018-06-07_19-54', 'rb'))
# index_to_tag = dict()
index_to_tag = pickle.load(open('dictionaries/index_to_tag_2018-06-07_19-54', 'rb'))

# for fname in static_files_list[:max_docs]:
#     for line in io.open(os.path.join(base_path, fname), "r", errors = 'ignore'):
#         splitline = line.lower().split()
#         if len(line) <= 1 or len(splitline) <= 2:
#             continue
#         vocab.update(splitline[0:1])
#         pos_tags.update(splitline[1:2])

# index = 1
# for token in vocab:
#     token_to_index[token] = index
#     index_to_token[index] = token
#     index += 1
#
# index = 0
# for tag in pos_tags:
#     tag_to_index[tag] = index
#     index_to_tag[index] = tag
#     index += 1
#
# print(tag_to_index)

################################################################################
####    Define and compile model    ############################################
print("Section: define and compile model")

model = K.models.Sequential()

#input_layer = kl.Input(shape=(1,))
#model.add(input_layer)

model.add(kl.Embedding(128, 64, input_length=30))
# model.add(kl.Embedding(len(vocab), 256, input_length=3))
model.add(kl.Flatten())
model.add(kl.Dense(len(pos_tags) + 5, activation="relu")) ###
model.add(kl.Dense(len(pos_tags), activation="softmax"))

model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["categorical_accuracy"])

print(model.summary())
################################################################################
####    Train model by iteratively reading tokens from input
print("Section: train model by iteratively reading tokens from input")

def token_to_index(token):
    token = token[:10]              # Take only first 10 chars
    token_array = []
    token += '`' * (10-len(token))  # Apply padding with char ASCII:96
    for char in token:
        if ord(char) <= 127:# and ord(char) >= 97:
            token_array += [ord(char)]
        else:
            token_array += [0]
    return (token_array)
token_to_index('astonishz')

def data_generator(start=0,end=max_docs):

    fist_token = [0] * 10; first_tag = 0
    second_token = [0] * 10; second_tag = 0
    third_token = [0] * 10; third_tag = 0

    for fname in static_files_list[start:end]:

        for line in io.open(os.path.join(base_path, fname), "r", errors = 'ignore'):

            splitline = line.lower().split()
            if len(line) <= 1 or len(splitline) <= 2:
                continue

            first_token = [] + second_token
            second_token = [] + third_token
            third_token = token_to_index(splitline[0])

            first_tag = second_tag
            second_tag = third_tag
            third_tag = tag_to_index[splitline[1]]

            # print(
            #     index_to_token.get(first_token,0),
            #     index_to_token.get(second_token,0),
            #     index_to_token.get(third_token,0), "\n",
            #     index_to_tag.get(first_tag,0),
            #     index_to_tag.get(second_tag,0),
            #     index_to_tag.get(third_tag,0), "\n"
            # )

            x_batch = numpy.zeros((1,30))
            x_batch[0] = numpy.array((first_token + second_token + third_token))

            y_batch = numpy.zeros((1,len(pos_tags)))
            y_batch[0,second_tag] = 1

            # print(x_batch)

            # print((x_batch, y_batch), "\n", (x_batch.shape, y_batch.shape))

            #yield (numpy.zeros((47,12)), numpy.zeros((12,1)))
            yield (x_batch, y_batch)

            #model.fit(x=x_batch, y=y_batch, batch_size=1, epochs=3, verbose=1)

# Train model using generator syntax
model.fit_generator(data_generator(), steps_per_epoch=1024, epochs=128)#, verbose=0)

x_batch = next(data_generator())[0]
print(model.predict(x_batch))

# Save model after done training

timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M')
model.save("alph_%s.kerasmodel" %(timestamp))

################################################################################
   # Evaluate model against remaining data
print("Section: evaluate model against remaining data")

print( model.evaluate_generator(data_generator(max_docs+1, len(static_files_list)), steps=100, workers=1) )
# model = K.Model()
