###############################################################################
####	Pramble    ############################################################
print("Section: preamble")

import keras as K
import keras
from keras import layers as kl
import numpy

import os
import io
#import progressbar

import time
import datetime

import pickle

################################################################################
####    Define and prepare input    ############################################
print("Section: define and prepare input")

max_docs = 10
base_path = "../spacy/tagged"
static_files_list = pickle.load(open('files_list.pickle', 'rb'))

vocab = set()
token_to_index = dict()
index_to_token = dict()

pos_tags = set()
tag_to_index = dict()
index_to_tag = dict()

for fname in static_files_list[:max_docs]:
    for line in io.open(os.path.join(base_path, fname), "r", errors = 'ignore'):
        splitline = line.lower().split()
        if len(line) <= 1 or len(splitline) <= 2:
            continue
        vocab.update(splitline[0:1])
        pos_tags.update(splitline[1:2])


print() # Newline

index = 1
for token in vocab:
    token_to_index[token] = index
    index_to_token[index] = token
    index += 1

index = 0
for tag in pos_tags:
    tag_to_index[tag] = index
    index_to_tag[index] = tag
    index += 1

print(tag_to_index)

################################################################################
####    Define and compile model    ############################################
print("Section: define and compile model")

# model = K.models.Sequential()
model = keras.models.load_model("model_2018-06-09_12-09.kerasmodel")
#input_layer = kl.Input(shape=(1,))
#model.add(input_layer)

# model.add(kl.Embedding(len(vocab), 256, input_length=3))
# model.add(kl.Flatten())
# #model.add(kl.Dense(len(vocab), activation="relu"))
# model.add(kl.Dense(len(pos_tags), activation="softmax"))
#
# model.compile(optimizer="adam", loss="categorical_crossentropy", metrics=["categorical_accuracy"])

print(model.summary())
################################################################################
####    Train model by iteratively reading tokens from input
# print("Section: train model by iteratively reading tokens from input")

def data_generator(start=0,end=max_docs):

    fist_token = 0; first_tag = 0
    second_token = 0; second_tag = 0
    third_token = 0; third_tag = 0

    for fname in static_files_list[start:end]:
        for line in io.open(os.path.join(base_path, fname), "r", errors = 'ignore'):

            splitline = line.lower().split()
            if len(line) <= 1 or len(splitline) <= 2:
                continue

            first_token = second_token
            second_token = third_token
            third_token = token_to_index.get(splitline[0],0)

            first_tag = second_tag
            second_tag = third_tag
            third_tag = tag_to_index.get(splitline[1],0)

            # print(
            #     index_to_token.get(first_token,0),
            #     index_to_token.get(second_token,0),
            #     index_to_token.get(third_token,0), "\n",
            #     index_to_tag.get(first_tag,0),
            #     index_to_tag.get(second_tag,0),
            #     index_to_tag.get(third_tag,0), "\n"
            # )

            x_batch = numpy.zeros((1,3))
            x_batch[0] = numpy.array((first_token, second_token, third_token))

            y_batch = numpy.zeros((1,len(pos_tags)))
            y_batch[0,second_tag] = 1

            #print((x_batch, y_batch), (x_batch.shape, y_batch.shape))

            #yield (numpy.zeros((47,12)), numpy.zeros((12,1)))
            yield (x_batch, y_batch)


# Train model using generator syntax
# model.fit_generator(data_generator(), steps_per_epoch=100, epochs=100)#, verbose=0)

# x_batch = next(data_generator())[0]
# print(model.predict(x_batch))

# Save model after done training

timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d_%H-%M')
# model.save("model_%s.kerasmodel" %(timestamp))

################################################################################
###    Evaluate model against remaining data
print("Section: evaluate model against remaining data")

print(model.metrics_names)

print(model.evaluate_generator(data_generator(), steps=100, workers=1))
#model = K.Model()

keras.backend.clear_session()
