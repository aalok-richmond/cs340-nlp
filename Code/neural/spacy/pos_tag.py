# Provided by tarnold2@UR

import spacy
import os
import io
#from progress_printer import Progress_printer as PP
import progressbar # requires pip3 package `progressbar2'
#pp = PP()

# User parameters
#base_path = "/Users/taylor/gh/federal-writer/transcripts" # TODO: Need to change this
base_path = "./../../../Data/Gutenberg/txt/"
max_docs = 100
oform = "{0:20s} \t {1:s} \t {2:s} \n"

# Load models and open output
nlp = spacy.load('en')


# Process each file and save results
file_names = os.listdir(base_path)
for fname in progressbar.progressbar(file_names[:max_docs]):
    with open("tagged/pos_tags_%s"%fname, 'a+') as fout:
        for line in io.open(os.path.join(base_path, fname), "r", errors = 'ignore'):# as ifile:
            #text = ifile.read().replace(u'\n', u' ')
            try:
                doc = nlp(line)
                for sent in doc.sents:
                    fout.writelines("\n")
                    for word in sent:
                        fout.writelines(oform.format(word.text, word.pos_, word.tag_))
            except:
                continue
#
