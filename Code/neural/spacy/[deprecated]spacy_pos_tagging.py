#!/bin/env python3
# Aalok S., 2018

import spacy, logging
import os
import progress_printer
#logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def counter(start=0):
    while True:
        yield start
        start += 1

### Read lines from several files in a memory-friendly manner
class LineReader:

    def __init__(self, dirname):
        self.dirname = dirname
        self.pp = progress_printer.Progress_printer()
        self.iteration = counter(1)

    def __iter__(self):
        print("\nIteration:\t%d" % next(self.iteration))
        total_num = len(os.listdir(self.dirname))
        iterator = iter(range(total_num))
        for file_name in os.listdir(self.dirname):
            self.pp.print_progress(next(iterator), total_num)
            #for line in
            yield open(os.path.join(self.dirname, file_name), 'r', encoding='latin1')
                #yield line.split()

class SpacyNLPWrapper:
    
    ## Self's SpaCy model instance. For retrospectively referencing and accessing
    model = None
    name = "empty_model"
    docs = dict()
    
    def __init__(self):
        self.nlp = spacy.load('en')
        self.docCounter = counter(1)
        
    def prep_data(self, origin_dir):
        """Gathers data and makes it into a trainable format"""
        for opened_file in LineReader(origin_dir):
            self.docs[docCounter()] = self.nlp(opened_file.read())
        
    def get_token_data(self):
        # train word2vec on the two sentences
        for doc in self.docs:
            for token in doc:
                print(token.text, token.lemma_, token.pos_, token.tag_, token.dep_,
                  token.shape_, token.is_alpha, token.is_stop)

    def dump(self, filepath):
        pass
    
    def load(self, filepath):
        pass
    
spacy = SpacyNLPWrapper()
spacy.prep_data("./../../../Data/Gutenberg/txt/")
spacy.get_token_data()

