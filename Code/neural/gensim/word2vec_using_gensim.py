#!/bin/env python3
# Aalok S., 2018

import gensim, logging
import os
import progress_printer
#logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

def counter(start=0):
    while True:
        yield start
        start += 1

### Read lines from several files in a memory-friendly manner
class LineReader:

    def __init__(self, dirname):
        self.dirname = dirname
        self.pp = progress_printer.Progress_printer()
        self.iteration = counter(1)

    def __iter__(self):
        print("\nIteration:\t%d" % next(self.iteration))
        total_num = int(.5*len(os.listdir(self.dirname)))
        iterator = iter(range(total_num))
        for file_name in os.listdir(self.dirname):
            nextiter = next(iterator)
            self.pp.print_progress(nextiter, total_num)
            if (nextiter >= total_num):
                print("\n\n")
                break
            for line in open(os.path.join(self.dirname, file_name), 'r', encoding='latin1'):
                yield line.split()

class GensimModelWrapper:
    
    ## Self's Gensim model instance. For retrospectively referencing and accessing
    model = None
    name = "empty_model"
    ## A data-storing object (usually a list of lists)
    data = [[],]
    min_count=100
    size=64
    
    def __init__(self):
        pass
    
    def prep_data(self, origin_dir):
        """Gathers data and makes it into a trainable format"""
        self.data = LineReader(origin_dir)
        
    def train(self, name):
        # train word2vec on the two sentences
        self.name = name
        self.model = gensim.models.Word2Vec(self.data, min_count=self.min_count, size=self.size, iter=3, workers=3)

    def dump(self, filepath):
        self.model.save(filepath)
    
    def load(self, filepath):
        self.model = gensim.models.Word2Vec.load(filepath)
        
    def get_gensim_model(self):
        return self.model
    
"""    
m1 = GensimModel()
data = [['first', 'sentence'], ['second', 'sentence']]
m1.train(data)
        
m2 = GensimModel()
m2.prep_data("./../../../Data/Gutenberg/txt/")
m2.train()
"""
