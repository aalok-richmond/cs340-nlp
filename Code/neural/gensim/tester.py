#! /bin/env/ python3
# Aalok S., 2018

import cmd
import itertools
from word2vec_using_gensim import GensimModelWrapper

def counter(start=0):
    while True:
        yield start
        start += 1

class Tester(cmd.Cmd):
        
    models = None
    current_model = None
    default_directory = "./../../../Data/Gutenberg/txt/"
        
    def __init__(self, *args):
        super(Tester, self).__init__()
        self.models = dict()
        self.counter = counter()
        
    def do_q(self, *args):
        """Exit."""
        raise SystemExit
        
    def do_make_model(self, *args):
        """Train a new model. Params: name, directory, vector_dim, word_min_count"""
        
        args = "" + str(args[0])
        
        name="%s_model%d"%(args,next(self.counter))
        self.models[name] = GensimModelWrapper()
        
        directory=self.default_directory
        
        self.models[name].size=256
        self.models[name].min_count=100
        self.current_model = name
        self.models[name].prep_data(directory)
        self.models[name].train(name)
        
        self.models[name].dump("model_%s_d=%d.gs" % (name, self.models[name].size))
        
    def do_train_further(self, *args):
        """Train existing model further"""
        args = str(args[0])
        try:
            name = self.current_model
            self.models[name].prep_data(args)
            self.models[name].train(name, self.models[name].size, self.models[name].min_count)
            self.models[name].dump("%s_d=%d.gs" % (name, self.models[name].size))
        except IOError:
            print("Couldn't access files from %s"%args)
        
    def do_load_model(self, *args):
        """Load model from saved location"""
        try:
            args = str(args[0])
            self.models[args] = GensimModelWrapper()
            self.models[args].load(args.split()[0])
            self.current_model = args
        except FileNotFoundError:
            print("File not found")
        
    def do_list_models(self, *args):
        """Show a list of currently trained models"""
        print(self.models.keys())
        
    def do_change_current_model(self, *args):
        """Select a different model to work with"""
        if str(args[0]) in self.models:
            self.current_model = str(args[0])
        else:
            raise NameError
                    
    def do_query_vector(self, *args):
        """Returns raw vector of a word"""
        try:
            print(self.models[self.current_model].get_gensim_model()[str(args[0])])
        except KeyError:
            print("No such model or word not found")
        
    def do_arithmetic(self, *args):
        """Query: p1+p2+p3+..+pn-n1-n2-...-nn, top N --> p1,p2,p3,..,pn~n1,n2,...nn~N"""
        groups = str(args[0]).split("~")
        positives = []
        negatives = []
        
        try:
            if len(groups) > 1:
                positives += groups[0].split(',')
        except IndexError:
            pass
        try:
            if len(groups) > 1:
                negatives += groups[1].split(',')
            
        except IndexError:
            pass
        
        try:
            print(self.models[self.current_model].get_gensim_model().most_similar(positive=positives, negative=negatives, topn=10))
        except Exception:
            print("Error, one or more words not found.", groups)

    def emptyline(self):
        pass

if __name__ == '__main__':
    testerCLI = Tester()
    testerCLI.prompt = '> '
    try:
        testerCLI.cmdloop()
    except KeyboardInterrupt:
        print()
        raise SystemExit
