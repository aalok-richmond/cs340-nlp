#! /bin/env/ python3
# Aalok S.

from cmd import Cmd
import progress_printer
import math

class Edit_Distance(Cmd) :

	costs = {'del' : 1., 'sub' : 2., 'ins' : 1.}
	progress_bar = None
	
	def __init__(self, *args):
		super(Edit_Distance, self).__init__()
		self.progress_bar = progress_printer.Progress_printer()
	
	def do_change_delete_cost(self, arg) :
		"""Change the delete cost to some new cost"""
		new_cost = float(input("Please enter a new cost: "))
		try :
			new_cost = 1.*(new_cost)
			self.costs['del'] = new_cost
		except :
			print("Please enter a valid numeric cost.")
	
	def do_change_insert_cost(self, arg) :
		"""Change the insert cost to some new cost"""
		new_cost = float(input("Please enter a new cost: "))
		try :
			new_cost = 1.*(new_cost)
			self.costs['ins'] = new_cost
		except :
			print("Please enter a valid numeric cost.")
			
	def do_change_substitution_cost(self, arg) :
		"""Change the substitution cost to some new cost"""
		new_cost = float(input("Please enter a new cost: "))
		try :
			new_cost = 1.*(new_cost)
			self.costs['sub'] = new_cost
		except :
			print("Please enter a valid numeric cost.")
			
	def do_show_current_costs(self, arg) :
		"""See the current operation costs for the edit distance algorithm"""
		print (self.costs)
		
	def do_get_distance(self, arg) :
		"""Computes the edit distance of supplied strings delimited by ~"""
		words = arg.split('~')
		#self.progress_bar.print_progress(0,len(words)**2)
		if len(words) < 2 :
			print ("Only one string supplied.")
			words.append('')
			
		matrix = [words,]
		counter = 0
		
		for string1 in words :
			distances = []
			for string2 in words :
				distances.append(self.levenshtein(string1, string2, self.costs))
				counter += 1
				#self.progress_bar.print_progress(counter,len(words)**2)
				
			matrix.append(distances)	
		
		print ('\n')
		words = [' '] + words
		for lineIndex in range(len(matrix)) :
			print (words[lineIndex],matrix[lineIndex])
					
	def do_help(self, arg) :
		"""Shows a usage message"""
		print("This program may be used to compute the edit distance between several strings.")
		print("To use, type `get_distance [word_1]~[word_2]~ ... [word_n]' (including the `~' characters).")
		print("To exit, type exit and hit return.")
		
	def do_exit(self, arg) :
		"""Exits the program."""
		print ("Quitting.")
		raise SystemExit
		
	def levenshtein(self, string1, string2, costs) :
	
		ld_matrix = {-1 : {-1 : 0}}
		
		for i in range(len(string1)) :
			ld_matrix[i] = {}
			for j in range(len(string2)) :
				ld_matrix[i][j] = 0#int(string1[i] != string2[j])
				ld_matrix[i][j] += min(costs['del']+ld_matrix.get(i-1,{}).get(j,math.inf),costs['ins']+ld_matrix.get(i,{}).get(j-1,math.inf),int(string1[i] != string2[j])*costs['sub']+ld_matrix.get(i-1,{}).get(j-1,math.inf))
		
		print (ld_matrix)
		return ld_matrix.get(len(string1)-1,{}).get(len(string2)-1,None)
		
if __name__ == '__main__':
    prompt = Edit_Distance("")
    prompt.prompt = ':: '
    try:
    	prompt.cmdloop('''
    Coded by Aalok S. on 2018-01-25.
    This program comes with ABSOLUTELY NO WARRANTY.
    This is free software, and you are welcome to redistribute it
    under certain conditions. See the GNU GPL v3.
    Usage: `get_distance [word_1]~[word_2]~ ... [word_n]' (including the `~' characters)''')
    except KeyboardInterrupt :
    	print("\nExiting due to KeyboardInterrupt")
    	raise SystemExit
