
###Stuff to work on

####*CS/NLP:*

1. Character --(embedding)--> intermediate repr --(dynamic_embedding)-->  equivalent of *d*-dimensional word
* Implicit backing-off to characters for unseen words
* semantics linguistics problem solver
* bag-of-characters model and vector sum. ref. https://arxiv.org/abs/1607.04606
* reconstructing PIE word senses using _all_ modern IE language (char) embeddings ref. https://lrc.la.utexas.edu/lex

---

####*Linguistics Problems*

1. Words --- Word vectors // complicated.
    * morpho-semantics problem
    * individually 3-5 dimensions only; otherwise make it into a team problem
    * Words (working language), words (different language), and vector representations
        * some way to encode morphology: similar morphemes (e.g., prefixes, suffixes) will have similar vector representations.
        * 
2. PIE reconstruction